var express = require('express');
var http = require('http');
const fs = require('fs');
const readline = require('readline');


const { Pool, Client } = require('pg');

const pool = new Pool({
	user: 'postgres',
	host: '35.188.175.9',
	database: 'labyrinth',
	password: 'ohk6shailiMu1ahw0Qui',
	port: 5432
})
//pool.connect()



pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})
var coldStorage = fs.readFileSync('/var/lib/labyrinth/webapp-backend/routes/AUTH_DB.json');
var users = JSON.parse(coldStorage);





var router = express.Router();
router.get('/', function(req, res, next) {
  res.json([]);
});
router.get('/verifywatchlist/:list', async (req, res, next) => {
	try{
		var l = req.params.list.split('-');
		var lOut = []
		for (var i=0; i < l.length; i++)
		{
			if (l[i] == ''){
			lOut.push('true')
			}else {
				var q = "SELECT * FROM labs WHERE lab LIKE \'%LOW-" + l[i] + ".lab\'"
				var result = await pool.query(q)
				if ( result.rows == undefined || result.rows.length == 0)
				{
					lOut.push('false')
				} else {
					
					lOut.push('true')
					console.log(result.rows[0])
				}
			}
		}
		res.json(lOut.join('-'))
	} catch(e){
		console.log(e)
		next(e)
	}
});
router.get('/currconfig/:schema', async (req, res, next) => {
	try{
//		const client = pool.connect()
		var data = req.params;
		var schema = data.schema;
    		const result = await pool.query('SELECT * FROM ' + schema + '.config;')
		res.json(result.rows[0])
//
	}catch(e){
		console.log(e)
		next(e)
	}
});
router.get('/currwatchlist/:schema', async (req, res, next) => {
        try{
                var data = req.params;
                var schema = data.schema;
                const result = await pool.query('SELECT * FROM ' + schema + '.watchlist;')
                res.json(result.rows[0])
//
        }catch(e){
		console.log(e)
                next(e)
        }
});router.get('/curruserinfo/:schema', async (req, res, next) => {
        try{
                var data = req.params;
                var schema = data.schema;
                const result = await pool.query('SELECT * FROM ' + schema + '.userinfo;')

                res.json(result.rows[0])
//
        }catch(e){
                next(e)
        }
});
router.get('/results/:schema', async (req, res, next) => {
        try{
                var data = req.params;
                var schema = data.schema;
                const result = await pool.query('SELECT * FROM ' + schema + '.results;')
                res.json(result.rows)
//
        }catch(e){
                next(e)
        }
});





router.get('/results/:schema/:matchid', async (req, res, next) => {

	var resultFile = "";

	try
	{
	   var data = req.params;
	   var schema = data.schema;
	   var matchid = data.matchid;

	   const result = await pool.query("SELECT * FROM " + schema + ".results WHERE id = '" + matchid +  "';")

	   resultFile = result.rows[0].result;


	}
	catch(e){ next(e) }

	var rawResults = require('fs').readFileSync(resultFile, 'utf-8')
	    .split('\n')
	    .filter(Boolean);
	
	res.json(rawResults);
});





//router.get('/user/:id', function (req, res, next) {
//  try {
    //const user = await getUserFromDb({ id: req.params.id })
    //res.json(user);

//  } catch (e) {
    //this will eventually be handled by your error handling middleware
//    next(e) 
//  }
//})

// Config related requests
var configs = [];
router.get('/updateconfig/', function(req, res, next) {
  
  		res.json(configs);
});

router.get('/updateconfig/:schema/:reflect/:density', function(req, res, next) {
  
		var newConfig = {};
  		var data = req.params;
 		newConfig["schema"] = data.schema;
  		newConfig["reflect"] = data.reflect;
  		newConfig["density"] = data.density;
  		configs.push(newConfig);
  		res.json([configs]);
});

router.get('/updateconfig/delete', function(req, res, next) {

		configs = [];
  		res.json([configs]);
});

//////////
// Watch list related requests
var watchlists = [];
router.get('/watchlist/', function(req, res, next) {
  
		res.json(watchlists);
});

router.get('/watchlist/:schema/:list/', function(req, res, next) {
        
		var newWatchlist = {};
  		var data = req.params;
  		newWatchlist["schema"] = data.schema;
  		newWatchlist["list"] = data.list;

  		watchlists.push(newWatchlist);
  		res.json([watchlists]);
});

router.get('/watchlist/delete', function(req, res, next) {
  
		watchlists = [];
  		res.json([watchlists]);
});

//////////
// New instance related requests
var newInstances = [];

router.get('/newinstance/', function(req, res, next) {

  		res.json(newInstances);
});

router.get('/newinstance/:schema/:email/:fname/:lname/:regkey', function(req, res, next) {
  
		var newInstance = {};
  		var data = req.params;
  		newInstance["schema"] = data.schema;
  		newInstance["email"] = data.email;
 		newInstance["fname"] = data.fname;
  		newInstance["lname"] = data.lname;
  		newInstance["regkey"] = data.regkey;

  		newInstances.push(newInstance);
  		//res.json([newInstances]);

		res.writeHead(302, {
	  		'Location': 'http://35.188.175.9:8000/user/login#/user/login'
		});
		res.end();

});

router.get('/newinstance/delete', function(req, res, next) {

		newInstances = [];
  		res.json([newInstances]);
});

//////////
// Users
//var users = [];

//router.get('/users/', function(req, res, next) {
//
//		res.json(users);
//		res.json([]);
//});

router.get('/updateuser/:authemail/:authpass/:schema/:fname/:lname/:email/', function(req, res, next) {
        var data = req.params;

        var decodedEmail = decodeURIComponent(data.email);
        var decodedPass = decodeURIComponent(data.pass);

        var decodedAuthEmail = decodeURIComponent(data.authemail);
        var decodedAuthPass = decodeURIComponent(data.authpass);


        for (var obj in users)
        {
                var storedEmail = users[obj].email;
                var storedPass = users[obj].password;

                if (decodedAuthEmail == storedEmail && decodedAuthPass == storedPass)
                {
                        users[obj].schema = data.schema;
                        users[obj].fname = data.fname;
                        users[obj].lname = data.lname;
                        users[obj].email = data.email;
                        //users[obj].password = data.pass;

                        var latestContents = JSON.stringify(users, null, 2);
                        console.log(latestContents);
                        fs.writeFileSync('/var/lib/labyrinth/webapp-backend/routes/AUTH_DB.json', latestContents);
                        res.json(["SUCCESS"]);
                        return;
                }
        }
        res.json([]);
});

router.get('/updateuser/:authemail/:authpass/:schema/:fname/:lname/:email/:pass', function(req, res, next) {
	var data = req.params;

        var decodedEmail = decodeURIComponent(data.email);
        var decodedPass = decodeURIComponent(data.pass);

        var decodedAuthEmail = decodeURIComponent(data.authemail);
        var decodedAuthPass = decodeURIComponent(data.authpass);


	for (var obj in users)
	{
		var storedEmail = users[obj].email;
                var storedPass = users[obj].password;

                if (decodedAuthEmail == storedEmail && decodedAuthPass == storedPass)
                {
			users[obj].schema = data.schema;
      			users[obj].fname = data.fname;
     			users[obj].lname = data.lname;
      			users[obj].email = data.email;
      			users[obj].password = data.pass;

		        var latestContents = JSON.stringify(users, null, 2);
			console.log(latestContents);
        		fs.writeFileSync('/var/lib/labyrinth/webapp-backend/routes/AUTH_DB.json', latestContents);
                        res.json(["SUCCESS"]);
			return;
                }
	}
	res.json([]);
});
// Auth
router.get('/users/:email/:pass', function (req, res, next) {

	var data = req.params;

	var decodedEmail = decodeURIComponent(data.email);
	var decodedPass = decodeURIComponent(data.pass);

	for(var obj in users)
	{
		var storedEmail = users[obj].email;
		var storedPass = users[obj].password;

		if (decodedEmail == storedEmail && decodedPass == storedPass)
		{
			res.json(users[obj]);
			return;
		}
	}

	res.json([]);

});

router.get('/users/:schema/:fname/:lname/:email/:password', function(req, res, next) {
 
 var regKey = '';
  fs.readFile('/var/lib/labyrinth/global/registration-keys', "utf8", (err, values) => {
    if (err) {
      console.log(err);
    } else {
      line = values.split("\n");
      line.length = 1;
      console.log(line);
      var newUserInfo = {};
      var data = req.params;
      newUserInfo["schema"] = data.schema;
      newUserInfo["fname"] = data.fname;
      newUserInfo["lname"] = data.lname;
      newUserInfo["email"] = data.email;
      newUserInfo["password"] = data.password;
      newUserInfo["regkey"] = line[0];

      users.push(newUserInfo);


      var latestContents = JSON.stringify(users, null, 2);
      console.log(latestContents);
      fs.writeFileSync('/var/lib/labyrinth/webapp-backend/routes/AUTH_DB.json', latestContents);


      res.json([newUserInfo]);
    }
  });

});

//////////
// Account changes related requests
var userinfos = [];

router.get('/userinfo/', function(req, res, next) {
  
		res.json(userinfos);
});

router.get('/userinfo/:schema/:fname/:lname/:email/', function(req, res, next) {
 

		var newUserInfo = {};
  		var data = req.params;
  		newUserInfo["schema"] = data.schema;
  		newUserInfo["fname"] = data.fname;
  		newUserInfo["lname"] = data.lname;
  		newUserInfo["email"] = data.email;

  		userinfos.push(newUserInfo);
  		res.json([userinfos]);
});

router.get('/userinfo/delete', function(req, res, next) {
  

		userinfos = [];
  		res.json([userinfos]);
});

// For sending new instance emails
var newEmails = [];

router.get('/confirmationemails/', function(req, res, next) {
  
		res.json(newEmails);
});
router.get('/confirmationemails/:email/:url', function(req, res, next) {
  
		var newEmail = {};
  		var data = req.params;
  		newEmail["email"] = data.email;
 		newEmail["url"] = data.url;

  		newEmails.push(newEmail);
  		res.json([newEmails]);
});

router.get('/confirmationemails/delete', function(req, res, next) {
  
		newEmails = [];
  		res.json([newEmails]);
});

module.exports = router;
