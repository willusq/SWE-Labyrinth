#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdlib.h>
#include <thread>

using namespace cv;
using namespace std;

string home;	
int density;
string labPath;
string rlabPath;

void getPath(Mat &frame,
             int frameNumber,
             uint8_t ***pathsArr,
             int REGION_SEPARATION,
             int REGION_LENGTH,
             int RES_X,
             int RES_Y)
{
    int currRed = 0, currGreen = 0, currBlue = 0, lastRed = 0, lastGreen = 0, lastBlue = 0;
    uint8_t identifyer = 0;
    
    for(int i = 1; i < RES_Y/REGION_SEPARATION; i++)
    {
        for(int j = 1; j < RES_X/REGION_SEPARATION; j++){
            for(int r = i*REGION_SEPARATION; r < i*REGION_SEPARATION+REGION_LENGTH; r++)
            {
                for(int c = j*REGION_SEPARATION; c < j*REGION_SEPARATION+REGION_LENGTH; c++)
                {
                    currRed   += frame.at<Vec3b>(r,c)[2];
                    currGreen += frame.at<Vec3b>(r,c)[1];
                    currBlue  += frame.at<Vec3b>(r,c)[0];
                }
            }
            
            if(currRed > lastRed)     { identifyer = identifyer | 1; }
            if(currGreen > lastGreen) { identifyer = identifyer | 2; }
            if(currBlue > lastBlue)   { identifyer = identifyer | 4; }
            
            pathsArr[frameNumber][i][j] = identifyer;
            identifyer = 0;
            
            lastRed   = currRed; currRed     = 0;
            lastGreen = currGreen; currGreen = 0;
            lastBlue  = currBlue; currBlue   = 0;
        }
    }
}

void getFlippedHorizontalPath(Mat &frame,
                              int frameNumber,
                              uint8_t ***pathsArr,
                              int REGION_SEPARATION,
                              int REGION_LENGTH,
                              int RES_X,
                              int RES_Y)
{
    int distanceFromRight = RES_X - (((RES_X/REGION_SEPARATION-1)*REGION_SEPARATION)+REGION_LENGTH);
    int shift = distanceFromRight - REGION_SEPARATION;
    
    int currRed = 0, currGreen = 0, currBlue = 0, lastRed = 0, lastGreen = 0, lastBlue = 0;
    uint8_t identifyer = 0;
    
    int jCounter = 1;
    
    for(int i = 1; i < RES_Y/REGION_SEPARATION; i++)
    {
        for(int j = (RES_X/REGION_SEPARATION-1); j > 0; j--){
            for(int r = i*REGION_SEPARATION; r < i*REGION_SEPARATION+REGION_LENGTH; r++)
            {
                for(int c = j*REGION_SEPARATION; c < j*REGION_SEPARATION+REGION_LENGTH; c++)
                {
                    currRed   += frame.at<Vec3b>(r,c+shift)[2];
                    currGreen += frame.at<Vec3b>(r,c+shift)[1];
                    currBlue  += frame.at<Vec3b>(r,c+shift)[0];
                }
            }
            
            if(currRed > lastRed)     { identifyer = identifyer | 1; }
            if(currGreen > lastGreen) { identifyer = identifyer | 2; }
            if(currBlue > lastBlue)   { identifyer = identifyer | 4; }
            
            pathsArr[frameNumber][i][jCounter] = identifyer;
            identifyer = 0;
            
            lastRed   = currRed; currRed     = 0;
            lastGreen = currGreen; currGreen = 0;
            lastBlue  = currBlue; currBlue   = 0;
            
            jCounter++;
        }
        
        jCounter=1;
    }
}


void writePathToFile(uint8_t ***paths,
			int frameCount,
			int REGION_SEPARATION,
			int REGION_LENGTH,
			int RES_X,
			int RES_Y,
			String name)
{
	ofstream myfile;
	if(density==0){
		myfile.open (home+"/labs/HIGH-"+name);
	}else if(density==1){
		myfile.open (home+"/labs/MED-"+name);
	}else if (density==2){
		myfile.open (home+"/labs/LOW-"+name);
	}else{
		return;
	}
	
	myfile << frameCount << endl;
	for(int i = 0; i < frameCount; i++)
	{
		
		for(int j = 0; j < RES_Y/REGION_SEPARATION; j++)
		{
			for(int k = 0; k < RES_X/REGION_SEPARATION; k++)
			{
				myfile << (int) paths[i][j][k];
				
			}
		}
		myfile << endl;
	}
	myfile.close();
	
}

void preprocessor(uint8_t ***sourcePaths,
                  int FRAME_COUNT,
                  int REGION_SEPARATION, 
                  int REGION_LENGTH, 
                  int RES_X, 
                  int RES_Y,
		  int TNum,
		  int ThreadCount,
                  bool backwards,
		  String name)
{
	Mat frame;
	VideoCapture source(home+"/mature-uploads/"+name);
	int framestart = FRAME_COUNT/ThreadCount * TNum;
	source.set(1,framestart);
	if(backwards)
		for(int i=framestart;i<(FRAME_COUNT/ThreadCount)+framestart; i++){
			source >> frame;
			//cout << i << endl;
			getPath(frame, i, sourcePaths, REGION_SEPARATION, REGION_LENGTH, RES_X, RES_Y);
 		
		}
	else
		for(int i=framestart;i<(FRAME_COUNT/ThreadCount)+framestart; i++){
			source >> frame;
			//cout << i << endl;
			getFlippedHorizontalPath(frame, i, sourcePaths, REGION_SEPARATION, REGION_LENGTH, RES_X, RES_Y);
			
		}
}
int main(int argc, char *argv[])
{	

	home = getenv("LAB_HOME");
	string name = argv[1];
	density = atoi(argv[2]);
	
	const int TARGET_REGION_LENGTH = 12;
	const int TARGET_REGION_SEPARATION = 30+density*3;
	const int TARGET_RES_X = 1920;
	const int TARGET_RES_Y = 1080;
	
	Mat frame;
	VideoCapture source(home+"/mature-uploads/"+name);
	
	const int RES_X = source.get(CAP_PROP_FRAME_WIDTH);
	const int RES_Y = source.get(CAP_PROP_FRAME_HEIGHT);
	
	const int steps=TARGET_RES_X/TARGET_REGION_SEPARATION;
	const int REGION_SEPARATION=RES_X/steps;

	
	const int reglen=TARGET_RES_X/TARGET_REGION_LENGTH;
	const int REGION_LENGTH=RES_X/reglen;
	const int FRAME_COUNT = source.get(CAP_PROP_FRAME_COUNT);
	
	uint8_t ***sourcePaths = new uint8_t**[FRAME_COUNT];
	for (int i = 0; i < FRAME_COUNT; i++) 
	{
		sourcePaths[i] = new uint8_t*[RES_Y/REGION_SEPARATION];
		for (int j = 0; j < RES_Y/REGION_SEPARATION; j++)
		{
			sourcePaths[i][j] = new uint8_t[RES_X/REGION_SEPARATION];
			for(int k=0;k<RES_X/REGION_SEPARATION;++k)
				sourcePaths[i][j][k]=0;
		}
	}
	
	uint8_t ***flipPaths = new uint8_t**[FRAME_COUNT];
	for (int i = 0; i < FRAME_COUNT; i++) 
	{
		flipPaths[i] = new uint8_t*[RES_Y/REGION_SEPARATION];
		for (int j = 0; j < RES_Y/REGION_SEPARATION; j++)
		{
			flipPaths[i][j] = new uint8_t[RES_X/REGION_SEPARATION];
			for(int k=0;k<RES_X/REGION_SEPARATION;++k)
				flipPaths[i][j][k]=0;
		}
	}
	
	auto begin = std::chrono::high_resolution_clock::now();
	
	unsigned threadCount = 6;
	
	thread threadArray[threadCount];
	
	for (int i = 0; i < threadCount; i++){
		threadArray[i] = thread(preprocessor, sourcePaths, FRAME_COUNT, REGION_SEPARATION, REGION_LENGTH, RES_X, RES_Y, i, threadCount, false, name);
	}
	
	for (int i = 0; i < threadCount; i++) 
		threadArray[i].join();
	
	
	auto end = chrono::high_resolution_clock::now();
	cout << chrono::duration_cast<chrono::nanoseconds>(end-begin).count()/1000000 << "ms" << endl;
	
	begin = std::chrono::high_resolution_clock::now();
	
	for (int i = 0; i < threadCount; i++){
		threadArray[i] = thread(preprocessor, flipPaths, FRAME_COUNT, REGION_SEPARATION, REGION_LENGTH, RES_X, RES_Y, i, threadCount, true, name);
	}
	
	for (int i = 0; i < threadCount; i++) 
		threadArray[i].join();
	
	end = chrono::high_resolution_clock::now();
	cout << chrono::duration_cast<chrono::nanoseconds>(end-begin).count()/1000000 << "ms" << endl;
	 
	
	writePathToFile(sourcePaths,FRAME_COUNT,REGION_SEPARATION,REGION_LENGTH,RES_X,RES_Y,name+".lab");
	writePathToFile(flipPaths,FRAME_COUNT,REGION_SEPARATION,REGION_LENGTH,RES_X,RES_Y,name+".rlab");


	string prefix = "";
	if(density == 0)
		prefix = "HIGH";
	else if (density == 1)
		prefix = "MED";
	else
		prefix = "LOW";	

	string lab = home + "/labs/" + prefix + "-" + name + ".lab";
	string rlab = home + "/labs/" + prefix + "-" + name + ".rlab";
	string checksum = name;
	string rs = to_string(REGION_SEPARATION);
	string rl = to_string(REGION_LENGTH);
	string resx = to_string(RES_X);
	string resy = to_string(RES_Y);
	string frames = to_string(FRAME_COUNT);

	string cmd = "PGPASSWORD='postgres' psql postgres -d labyrinth -c \"INSERT INTO LABS(lab, rlab, checksum, rs, rl, resx, resy, frames) VALUES('" +lab+ "', '" +rlab+ "', '" +checksum+ "', '" +rs+ "', '" +rl+ "', '" +resx+ "', '" +resy+ "', '" +frames+ "');\";";
	system(cmd.c_str());

	return 0;
	}
