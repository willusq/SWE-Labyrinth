#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdlib.h>
#include <thread>
#include <unordered_map>

using namespace cv;
using namespace std;

struct result
{
    int source;
    int other;
    float likeness;
    int offset;
};

// Holy number of paramters, robin!
float compareFrames(uint8_t ***sourcePaths, 
                    uint8_t ***otherPaths,
                    int sourceFrame,
                    int otherFrame,
                    int REGION_SEPARATION,
                    int RES_X,
                    int RES_Y)
{
    int matches = 0;
    
    for(int i = 0; i < RES_Y/REGION_SEPARATION; i++)
    {
        for(int j = 0; j < RES_X/REGION_SEPARATION; j++)
        {
            if(sourcePaths[sourceFrame][i][j] == otherPaths[otherFrame][i][j])
                matches++;
        }
    }
    
    return (float)(matches) / (float)((RES_Y/REGION_SEPARATION) * (RES_X/REGION_SEPARATION));
}

// Holy nubmer of paramters, batman!
void compareFrameBatch(uint8_t ***sourcePaths, 
                       uint8_t ***otherPaths,
                       result *resultsArr,
                       int sourceStart,
                       int otherStart,
                       int startIndex,
                       int numThreads,
                       int REGION_SEPARATION,
                       int FRAME_COUNT_SOURCE,
                       int FRAME_COUNT_OTHER,
                       int RES_X,
                       int RES_Y)
{
    
    for(int sourceFrame = startIndex; sourceFrame < FRAME_COUNT_SOURCE; sourceFrame += numThreads)
    {
        float best = 0.0;
        int bestIndex = -1;
        for(int otherFrame = 0; otherFrame < FRAME_COUNT_OTHER; otherFrame++)
        {
            float result = compareFrames(sourcePaths, otherPaths, sourceFrame, otherFrame, REGION_SEPARATION, RES_X, RES_Y);
            if(result > best)
            {
                best = result;
                bestIndex = otherFrame;
            }            
        }
        
        resultsArr[sourceFrame].source = sourceFrame + sourceStart;
        resultsArr[sourceFrame].other = bestIndex + otherStart;
        resultsArr[sourceFrame].likeness = best;
        resultsArr[sourceFrame].offset = ((bestIndex+otherStart)-(sourceFrame+sourceStart));
        
    }
}

void readPathFromFile(uint8_t ***paths,
			int frameCount,
			int REGION_SEPARATION,
			int RES_X,
			int RES_Y,
			int frameStart,
			String name)
{
	ifstream myfile;string line;
	myfile.open (name);
	
	for(int i = 0; i < frameCount; i++)
	{
		line="";
		getline(myfile,line);
		int count = 0;
		for(int j = 0; j < RES_Y/REGION_SEPARATION; j++)
		{
			for(int k = 0; k < RES_X/REGION_SEPARATION; k++)
			{
				char tmp = line[count];
				stringstream strValue;
				strValue << tmp;
				unsigned int intValue;
				strValue >> intValue;
				paths[i][j][k] =  (uint8_t) intValue;
				count++;
				
			}
		}
	}
	myfile.close();
	
}
int getLenLab(String name)
{
	ifstream myfile;
	string line;
	myfile.open (name);
	getline(myfile,line);
	int i=atoi(line.c_str());
	myfile.close();
	return i;
	
}

void writeResultsToFile(result *resArr,
				int frameCount,
				String name){
	ofstream f;
	f.open(name);
	for(int i=0;i<frameCount;i++){
		f << (int) resArr[i].source << "," << (int) resArr[i].other << "," << (float) resArr[i].likeness << "," << (int) resArr[i].offset << endl;
	}
}

int main(int argc, char *argv[])
{
    string home = getenv("LAB_HOME");

    if(argc != 9)
    {					// 		0				1		2		3						4		5			6			
        cout << endl << "USAGE: ./matching-algorithm   density   schema   'otherLab'   'space serparated source labs'   #of-source-labs   id(highest in result table+1)   ext(rlab,lab)   coreCount" << endl << endl;
        return -1;
    }
	int density = atoi(argv[1]);
	string schema = argv[2];
	string otherRawName = argv[3];
    int arg2count = atoi(argv[5]);
    int resID = atoi(argv[6]);
    string ext = argv[7];
    int coreCount = atoi(argv[8]);
    
    string labArr[arg2count];
    int x = 0;
    stringstream ssin(argv[4]);
    while (ssin.good() && x < arg2count){
        ssin >> labArr[x];
        ++x;
    }
    

    
    for(int src = 0; src < arg2count; src++)
    {
    	cout << labArr[src] << endl;
	    string sourceRawName = labArr[src];
	    string otherLab;
	    string sourceLab;
	    string sourceRLab;
	    if(density==0){
		otherLab = home+"/labs/HIGH-"+otherRawName+".lab";
		sourceLab = home+"/labs/HIGH-"+sourceRawName+"."+ext;
	    }else if(density==1){
		otherLab = home+"/labs/MED-"+otherRawName+".lab";
		sourceLab = home+"/labs/MED-"+sourceRawName+"."+ext;
	    }else if(density==2){
		otherLab = home+"/labs/LOW-"+otherRawName+".lab";
		sourceLab = home+"/labs/LOW-"+sourceRawName+"."+ext;
	    }else{
		cout << "Valid density's are between 0 and 2"<<endl;
		    return -1;
		}

	    int sourceStart = 0;
	    int otherStart = 0;
	    
	    // Parameters to be set at runtime
	    // Yes you can set them at runtime once, I checked
	    const int REGION_LENGTH = 12;
	    const int REGION_SEPARATION = 30+density*3;
	    const int RES_X = 1920;
	    const int RES_Y = 1080;
	    const int FRAME_COUNT_SOURCE = getLenLab(sourceLab);
	    const int FRAME_COUNT_OTHER  = getLenLab(otherLab);

	    Mat frame;
	    
	    // Prefill for source
	    uint8_t ***sourcePaths = new uint8_t**[FRAME_COUNT_SOURCE];
	    for (int i = 0; i < FRAME_COUNT_SOURCE; i++) 
	    {
	        sourcePaths[i] = new uint8_t*[RES_Y/REGION_SEPARATION];
	        for (int j = 0; j < RES_Y/REGION_SEPARATION; j++)
	            sourcePaths[i][j] = new uint8_t[RES_X/REGION_SEPARATION];
	    }
	    readPathFromFile(sourcePaths,FRAME_COUNT_SOURCE,REGION_SEPARATION,RES_X,RES_Y,sourceStart,sourceLab);
	    
	    // Prefill for other
	    uint8_t ***otherPaths = new uint8_t**[FRAME_COUNT_OTHER];
	    for (int i = 0; i < FRAME_COUNT_OTHER; i++) 
	    {
	        otherPaths[i] = new uint8_t*[RES_Y/REGION_SEPARATION];
	        for (int j = 0; j < RES_Y/REGION_SEPARATION; j++)
	            otherPaths[i][j] = new uint8_t[RES_X/REGION_SEPARATION];
	    }
	    readPathFromFile(otherPaths,FRAME_COUNT_OTHER,REGION_SEPARATION,RES_X,RES_Y,otherStart,otherLab);
	    
	    
	    result* resultsArr;
	    resultsArr = new result[FRAME_COUNT_SOURCE];
	    
	    unsigned threadCount = coreCount;
	    thread threadArray[threadCount];
	    for (int i = 0; i < threadCount; i++)
	        threadArray[i] = thread(compareFrameBatch, sourcePaths, otherPaths, resultsArr, sourceStart, otherStart, i, threadCount, REGION_SEPARATION, FRAME_COUNT_SOURCE, FRAME_COUNT_OTHER, RES_X, RES_Y);
	    for (int i = 0; i < threadCount; i++) 
	        threadArray[i].join();

	    

		///  Postprocessing be yonder ///



	    // Following section finds the 3 largest stretches
	    // of the source video that were ripped
	    unordered_map<int, int> offsetMap;
	    unordered_map<int, int> countMap;
	    unordered_map<int, int> offsetStartPoints;
	    
	    for(int i = 0 ; i < FRAME_COUNT_SOURCE; i++)
	    {
	        offsetMap[resultsArr[i].offset]++;

	        if(offsetStartPoints.find(resultsArr[i].offset) == offsetStartPoints.end())
	        	offsetStartPoints[resultsArr[i].offset] = resultsArr[i].source;
	    }

    	int i = 0;
	    for (auto it = offsetMap.begin(); it != offsetMap.end(); it++)
	    {
	        countMap[it->second] = it->first;
	        i++;
	    }

	    int sortedOffsetCounts[countMap.size()] = {0};

	    i = 0;
	   	for (auto it = countMap.begin(); it != countMap.end(); it++)
	    {
	       	sortedOffsetCounts[i] = it->first;
	        i++;
	    }


	    sort(sortedOffsetCounts, sortedOffsetCounts + (sizeof(sortedOffsetCounts)/sizeof(sortedOffsetCounts[0])));


		int tops = 0;
		string summary = ""; 

		for(int i = sizeof(sortedOffsetCounts)/sizeof(sortedOffsetCounts[0])-1; tops < 3; i--)
		{
			// Length of the stretch (10 second cutoff)
			if(sortedOffsetCounts[i] >= 240 && (sortedOffsetCounts[i] < (FRAME_COUNT_SOURCE + 1)))
			{

				int beginningOfSourceStretch = offsetStartPoints[countMap[sortedOffsetCounts[i]]];
				int endOfSourceStretch = beginningOfSourceStretch + sortedOffsetCounts[i];
				int beginningOfSourceStretchSeconds = beginningOfSourceStretch/24;
				int endOfSourceStretchSeconds = endOfSourceStretch/24;

				int beginningOfOtherStretch = beginningOfSourceStretch + countMap[sortedOffsetCounts[i]];
				int endOfOtherStretch = endOfSourceStretch + countMap[sortedOffsetCounts[i]];
				int beginningOfOtherStretchSeconds = beginningOfOtherStretch/24;
				int endOfOtherStretchSeconds = endOfOtherStretch/24;


				summary += to_string(beginningOfSourceStretchSeconds) + "-" + to_string(endOfSourceStretchSeconds) + "|" + to_string(beginningOfOtherStretchSeconds) + "-" + to_string(endOfOtherStretchSeconds) + ",";
			}

			tops++;
		}

		if(summary != "")
		{
			string resultsFileName = home+"/results/" + sourceRawName + "__" + otherRawName + ".res" + to_string(resID);
			cout << "Wrote results to " + home+"/results/" + sourceRawName + "__" + otherRawName + ".res" << endl;
			writeResultsToFile(resultsArr, FRAME_COUNT_SOURCE, resultsFileName);

			string cmd = "PGPASSWORD='postgres' psql postgres -d labyrinth -c \\\"INSERT INTO " +schema+ ".results(id, result, sections, source, other) VALUES('" +to_string(resID)+ "', '" +resultsFileName+ "', '" +summary+ "', '" +sourceRawName+ "', '" +otherRawName+ "');\\\"";

			cmd = "su - postgres -c \"" + cmd + "\"";
			system(cmd.c_str());
			string notification="$LAB_HOME/scripts/notification.sh " + schema + " " + to_string(resID);
			system(notification.c_str());
			++resID;
		}

	}

    return 0;

}

