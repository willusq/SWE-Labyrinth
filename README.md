### Required Components
[OpenCV 3.4.0](https://opencv.org)

[jsawk](https://github.com/micha/jsawk)

[Postgresql 9.6.0](https://www.postgresql.org/)

[Node.js](https://nodejs.org/en/)

[Postfix](http://www.postfix.org/)
## File Structure
### bin
This is the directory where the compiled binaries are stored
### global
This is the directory where the labyrinth queue, and the registration key files are stored
### labs
This is the directory where the processed labs are stored after the preprocessor
### matureUploads
This is the directory where the video files are stored after being added to the queue
### results
This is the directory where the processed results files are kept
### scripts
This directory contains all of the daemon scripts, as well as the start and kill scripts for both the daemon and web-app
### src
This directory contains the c++ files for preprocessor and matching algorithm
### uploads
This is the directory where video files are added to start the preprocessing and matching
### web-app
This is the directory that contains the entire front end website
### webapp-backend
This is the directory that contains the backend of the website, the API
# How to tell if the three major components are up and running
These commands should be run as root, by running:
```
sudo su
```
The default directory of labyrinth can be reached with the following command:
```
cd $LAB_HOME
```

## 1) For the collection of daemon scripts, also known as 'Labyrinth Server':
running this command from the shell:
```
  ps ax | grep '.sh' 
```
should show output containing these scripts (in no particular order):
```
/bin/bash /var/lib/labyrinth/scripts/configWatchdog.sh
/bin/bash /var/lib/labyrinth/scripts/instanceMaster.sh
/bin/bash /var/lib/labyrinth/scripts/confirmation-emails.sh
/bin/bash /var/lib/labyrinth/scripts/upload-watchdog-part1.sh
/bin/bash /var/lib/labyrinth/scripts/upload-watchdog-part2.sh
```
## 2) For the WebApp frontend:
running this command from the shell should return serveral process id's:
```
  pgrep node
```

## 3) For the WebApp backend:
running this command from the shell should return one process id:
```
  pgrep -f www
```

# How to stop and start the three major components:

NOTE: Make sure these components are not already running before trying to start them.

## 1) For the collection of daemon scripts, also known as 'Labyrinth Server':
stop these scripts by running this command in the scripts directory
```
  ./kill-all.sh   
```
start these scripts by running this command in the scripts directory
```  
  ./start-all.sh
```

## 2) For the WebApp frontend and the WebApp backend:
stop these systems by running this command in the scripts directory
```
  ./webapp-kill.sh   
```

start these systems by running this command in the scripts directory
```
  ./webapp-start.sh
```

### Example of full-lifecycle usage for Labyrinth

1) Navigate to wit-labyrinth.tk and register for an account

2) Click the confirmation link sent via email to complete registration
   (this unlocks the actual ability to be recognized by labyrinth server for resource allocation when new uploads are processed)

3) Log in with the previously entered credentials

4) Click the button in the top right hand corner, and click 'help' to read a brief tutorial

5) Configure your instance however you would like, the videos that can be entered to the watch list
   at any given time is determined by the contents of the labs/ directory, as any entries in that directory signify
   videos that have already been preprocessed and can be understood by the matching-algorithm binary


At this point, to actually see the software working as intended you would need to have at least one video in the watch list.
A quick example to see all the systems in action has been provided below:


6) Commit   bluePlanet   to your watch list

7) Simulate a new video upload by moving the video titled   mashup   from the mature-uploads directory to the uploads directory

8) Run the shell command   top   to view the active system processes that are using significant resources (Type CTRL-C to quit when finished)

9) Observe the preprocessor running in top

10) Observe the matching-algorithm running in top

11) Observe that an email notification is sent regarding the recent detection (bluePlanet is one of many videos concatenated in the one called mashup)

12) Return to the WebApp dashboard and observe recent detection is present; view more details/analytics by following the guide in the WebApp help section


### Expected Behavior for Detections

Provided in the mature-uploads directory are a variety of videos.
For testing purposes, commit videos to the watch list that are part of the video called   mashup.
Then, move mashup to uploads to simulate a new video upload to the server. The contents of mashup are given below:


The first minute contains the video titled   shaunWhiteOlympics

Minutes two-three contain the video titled   bluePlanet

Minutes three-4.5 contain the video titled   bigBuckBunny

Minutes 4.5-7 contain a horizontally flipped video titled   readyPlayerOneTrailer  (and thus will only be detected for infringements should 'check for reflections' be toggled in a user's WebApp settings)
