import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux, Link } from 'dva/router';
import { Form, Input, Button, Select, Row, Col, Popover, Progress } from 'antd';
import styles from './Register.less';
import axios from 'axios';

const FormItem = Form.Item;
const { Option } = Select;
const InputGroup = Input.Group;

const passwordStatusMap = {
  ok: <div className={styles.success}>Success</div>,
  pass: <div className={styles.warning}>Normal</div>,
  poor: <div className={styles.error}>Poor</div>,
};

const passwordProgressMap = {
  ok: 'success',
  pass: 'normal',
  poor: 'exception',
};

@connect(({ register, loading }) => ({
  register,
  submitting: loading.effects['register/submit'],
}))
@Form.create()
export default class Register extends Component {
  state = {
    count: 0,
    confirmDirty: false,
    visible: false,
    help: '',
    prefix: '86',
    firstName: '',
    lastName: '',
    emailAddress: '',
    password: ''
  };

  componentWillReceiveProps(nextProps) {
    const account = this.props.form.getFieldValue('mail');
    if (nextProps.register.status === 'ok') {
      this.props.dispatch(routerRedux.push({
        pathname: '/user/register-result',
        state: {
          account,
        },
      }));
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  onGetCaptcha = () => {
    let count = 59;
    this.setState({ count });
    this.interval = setInterval(() => {
      count -= 1;
      this.setState({ count });
      if (count === 0) {
        clearInterval(this.interval);
      }
    }, 1000);
  };

  getPasswordStatus = () => {
    const { form } = this.props;
    const value = form.getFieldValue('password');
    if (value && value.length > 9) {
      return 'ok';
    }
    if (value && value.length > 5) {
      return 'pass';
    }
    return 'poor';
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields({ force: true }, (err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'register/submit',
          payload: {
            ...values,
            prefix: this.state.prefix,
          },
        });
      }
    });
  };

  handleConfirmBlur = (e) => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  checkConfirm = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('The passwords do not match.');
    } else {
      callback();
    }
  };

  checkPassword = (rule, value, callback) => {
    if (!value) {
      this.setState({
        help: 'Please enter a password. ',
        visible: !!value,
      });
      callback('error');
    } else {
      this.setState({
        help: '',
      });
      if (!this.state.visible) {
        this.setState({
          visible: !!value,
        });
      }
      if (value.length < 6) {
        callback('error');
      } else {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      }
    }
  };

  changePrefix = (value) => {
    this.setState({
      prefix: value,
    });
  };

  renderPasswordProgress = () => {
    const { form } = this.props;
    const value = form.getFieldValue('password');
    const passwordStatus = this.getPasswordStatus();
    return value && value.length ? (
      <div className={styles[`progress-${passwordStatus}`]}>
        <Progress
          status={passwordProgressMap[passwordStatus]}
          className={styles.progress}
          strokeWidth={6}
          percent={value.length * 10 > 100 ? 100 : value.length * 10}
          showInfo={false}
        />
      </div>
    ) : null;
  };

  onChangeFirstName = (e) => {
    this.setState({ firstName: e.target.value });
  }

  onChangeLastName = (e) => {
    this.setState({ lastName: e.target.value });
  }

  onChangeEmailAddress = (e) => {
    this.setState({ emailAddress: e.target.value });
  }

  onChangePassword = (e) => {
    this.setState({ password: e.target.value });
  }

  registerUser = () => {
    if (this.state.firstName != '' & this.state.lastName != '' & this.state.emailAddress != '' & this.state.password != '') {
    var email = this.state.emailAddress.split("@");
    var domain = email[1].replace('.', '');
    email = email[0] + domain;
    email = 'schema_' + email.replace('@', '');
    console.log(email);
    var fname = this.state.firstName;
    var lname = this.state.lastName;
    var password = this.state.password;
    var url = 'http://35.188.175.9:7000/api/users/' + email + '/' + fname + '/' + lname + '/' + this.state.emailAddress + '/' + password;
    axios.get(url)
    .then((response) => {
      console.log(response);
      axios.get('http://35.188.175.9:7000/api/confirmationemails/' + encodeURIComponent(this.state.emailAddress) + '/' + encodeURIComponent('http://35.188.175.9:7000/api/newinstance/' + email + '/' + encodeURIComponent(this.state.emailAddress) + '/' + fname + '/' + lname + '/' + response.data[0].regkey))
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error)
      })
    })
    .catch((error) => {
      console.log(error)
    })	
    } 
  }

  render() {
    const { form, submitting } = this.props;
    const { getFieldDecorator } = form;
    const { count, prefix } = this.state;
    return (
      <div className={styles.main}>
        <h3></h3>
        <Form onSubmit={this.handleSubmit}>
          <FormItem>
            {getFieldDecorator('fname', {
              rules: [
                {
                  required: true,
                  message: 'Please enter your first name.',
                },
              ],
            })(<Input onChange={this.onChangeFirstName} size="large" placeholder="First Name" />)}
          </FormItem>
          <FormItem>
	    {getFieldDecorator('lname', {
              rules: [
                {
                  required: true,
                  message: 'Please enter your last name.',
                },
              ],
            })(<Input onChange={this.onChangeLastName} size="large" placeholder="Last Name" />)}
          </FormItem>
          <FormItem>
            {getFieldDecorator('mail', {
              rules: [
                {
                  required: true,
                  message: 'Please enter an email address.',
                },
                {
                  type: 'email',
                  message: 'Email address format is incorrect.',
                },
              ],
            })(<Input onChange={this.onChangeEmailAddress} size="large" placeholder="Email Address" />)}
          </FormItem>
          <FormItem help={this.state.help}>
            <Popover
              content={
                <div style={{ padding: '4px 0' }}>
                  {passwordStatusMap[this.getPasswordStatus()]}
                  {this.renderPasswordProgress()}
                  <div style={{ marginTop: 10 }}>
                    Password
                  </div>
                </div>
              }
              overlayStyle={{ width: 240 }}
              placement="right"
              visible={this.state.visible}
            >
              {getFieldDecorator('password', {
                rules: [
                  {
                    validator: this.checkPassword,
                  },
                ],
              })(
                <Input
                  size="large"
                  type="password"
                  placeholder="Password"
                />
              )}
            </Popover>
          </FormItem>
          <FormItem>
            {getFieldDecorator('confirm', {
              rules: [
                {
                  required: true,
                  message: 'Please confirm your password.',
                },
                {
                  validator: this.checkConfirm,
                },
              ],
            })(<Input onChange={this.onChangePassword} size="large" type="password" placeholder="Confirm Password" />)}
          </FormItem>
          <FormItem>
            <Button
              size="large"
              loading={submitting}
              className={styles.submit}
              type="primary"
              htmlType="submit"
              onClick={this.registerUser}
            >
              Sign Up
            </Button>
            <p>Already have an account?
            <Link className={styles.login} to="/user/login">
              Login
            </Link>
            </p>
          </FormItem>
        </Form>
      </div>
    );
  }
}
