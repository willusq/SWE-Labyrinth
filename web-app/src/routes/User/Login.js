import React, { Component, Text, View } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Checkbox, Alert, Icon } from 'antd';
import Login from '../../components/Login';
import styles from './Login.less';
import axios from 'axios';

const { Tab, UserName, Password, Mobile, Captcha, Submit } = Login;

@connect(({ login, loading }) => ({
  login,
  submitting: loading.effects['login/login'],
}))
export default class LoginPage extends Component {
  state = {
    type: 'account',
    autoLogin: true,
    error: false,
    foundUser: false
  }

  onTabChange = (type) => {
    this.setState({ type });
  }

  handleSubmit = (err, values) => {
    const { type } = this.state;
    var url = 'http://35.188.175.9:7000/api/users/' + encodeURIComponent(values.userName) + '/' + encodeURIComponent(values.password);
    this.renderMessage();
    axios.get(url)
    .then((response) => {
      console.log(values)
        
	 if (typeof(response.data.schema) != 'undefined') {
          this.setState({foundUser: true});
          console.log(response);
	
          values['fname'] = response.data.fname;
          values['lname'] = response.data.lname;
          values['schema'] = response.data.schema;
          values['fullname'] = response.data.fname + " " + response.data.lname;
          delete values.password;
          localStorage.setItem('user', JSON.stringify(values))
          this.setState({error: false});
	  var tempLogin = {};
          tempLogin['userName'] = 'admin';
          tempLogin['password'] = '888888';
          if (!err) {
            this.props.dispatch({
              type: 'login/login',
              payload: {
                ...tempLogin,
                type,
              },
            });
          }
        }

      if (this.state.foundUser == false) {
        this.setState({error: true});
      }
    })
    .catch((error) => {
      console.log(error)
    })
  }

  changeAutoLogin = (e) => {
    this.setState({
      autoLogin: e.target.checked,
    });
  }

  renderMessage = (content) => {
    return (
      <Alert style={{ marginBottom: 24 }} message={content} type="error" showIcon />
    );
  }

  render() {
    const { login, submitting } = this.props;
    const { type, error } = this.state;
    return (
      <div className={styles.main}>
        <Login
          defaultActiveKey={type}
          onTabChange={this.onTabChange}
          onSubmit={this.handleSubmit}
        >
          <UserName style={{marginTop: 64}} name="userName" placeholder="Username" />
          <Password name="password" placeholder="Password" />
          <Submit loading={submitting}>Login</Submit>
          <div className={styles.other}>
            <p>Need an account? <Link to="/user/register">Register</Link></p>
          </div>
        </Login>
        {error ?
          (<Alert style={{ marginBottom: 24 }} message={'Username or password was incorrect.'} type="error" showIcon />) :
          (<div></div>)
        }
      </div>
    );
  }
}
