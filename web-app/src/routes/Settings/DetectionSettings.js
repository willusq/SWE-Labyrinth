import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Modal, Spin, Card, Button, Input, Form, InputNumber, Switch, Select } from 'antd';
const FormItem = Form.Item;
import axios from 'axios';
import { routerRedux } from 'dva/router';
import store from '../../index';

export default class DetectionSettings extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
       defaultReflect: false,
       defaultDensity: 0,
       loading: true,
       visible: false
    }
    this.onDensityChange = this.onDensityChange.bind(this);
    this.onReflectionChange = this.onReflectionChange.bind(this);
    this.updateConfig = this.updateConfig.bind(this);
  }
  
  componentDidMount() {
    var userData = JSON.parse(localStorage.getItem('user'));
    var url = 'http://35.188.175.9:7000/api/currconfig/' + userData.schema;
    axios.get(url)
    .then((response) => {
	this.setState({defaultReflect: response.data.reflection})
	this.setState({defaultDensity: response.data.quality})
    });
    this.setState({loading: false})
  }

  onDensityChange(value) {
    console.log(value);
    this.setState({defaultDensity: value});
  }

  onReflectionChange(value) {
    console.log(value);
    this.setState({defaultReflect: value});
  }

  updateConfig() {
    var reflectValue = this.state.defaultReflect;
    var densityValue = this.state.defaultDensity;
    var userData = JSON.parse(localStorage.getItem('user'));
    var url = 'http://35.188.175.9:7000/api/updateconfig/'+ userData.schema +'/' + reflectValue + '/' + densityValue + '/';

    axios.get(url)
    .then((response) => {
      console.log(response)
      this.success();
    })
    .catch((error) => {
      console.log(error)
    })
  }

  success = () => {
    const { dispatch } = store;
    Modal.success({
      title: 'Detection settings were successfully updated!',
      onOk() {
        dispatch(routerRedux.push('/dashboard/videos'));
      }
    });
  }  

  render() {
   const { defaultReflect, defaultDensity } = this.state; 

   return (
      <div>
	<Spin spinning={this.state.loading}>
        <Card bordered={true}>
          <div style={{display: 'flex', flexDirection: 'row'}}>
            <h3>{'Detection Settings'}</h3>
            <Button onClick={this.updateConfig} style={{marginLeft: 'auto'}} type="primary">{"Save"}</Button>
          </div>
          <div style={{display: 'flex', flexDirection: 'row', marginTop: 16}}>
            <FormItem
            label="Density"
            >
            </FormItem>
            <Select defaultValue={ "1"  } value={ String(defaultDensity) } style={{ marginTop: 4 }} onChange={this.onDensityChange}>
              <Select.Option value="2">Low</Select.Option>
              <Select.Option value="1">Medium</Select.Option>
              <Select.Option value="0">High</Select.Option>
            </Select>
          </div>
          <div style={{display: 'flex', flexDirection: 'row'}}>
            <FormItem
              label="Detect reflections in media"
            ></FormItem>
            <Switch defaultValue={ false } checked={ defaultReflect } onChange={this.onReflectionChange} style={{marginTop: 8}} />
          </div>
        </Card>
	</Spin>
      </div>
    );
  }
}
