import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Card } from 'antd';

export default class SelectSetting extends PureComponent {
  state = {
  };

  render() {

    return (
      <div>
        <Card bordered={true}>
          {'Watch List'}
        </Card>
        <Card style={{marginTop: 16}} bordered={true}>
          {'Detection Settings'}
        </Card>
        <Card style={{marginTop: 16}} bordered={true}>
          {'User Settings'}
        </Card>
      </div>
    );
  }
}
