import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Spin, Card, Button, Input, Form, InputNumber, Switch } from 'antd';
const FormItem = Form.Item;
import axios from 'axios';

export default class Help extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div>
        <Card bordered={true}>
          <div>
            <h1>{'Help'}</h1>
	    <br></br>
	    <h3>{'Watchlist'}</h3>
	    <p>{'Use these fields to define which videos you would like to monitor for infringements.'}</p>
	    <br></br>
	    <h3>{'Detections Settings'}</h3>
	    <p>{'Turning off the reflection switch will double detection performance, but horizontally reflected infringements will not be detected.'}</p>
	    <p>{'The quality dropdown offers marginal increases to performance.'}</p>
	    <p>{'Setting the quality to "High" will lower performance but increase detection.'}</p>
	    <br></br>
	    <h3>{'User Settings'}</h3>
	    <p>{'Use these fields to make changes to your user information.'}</p>
	    <br></br>
	    <h3>{'Dashboard'}</h3>
	    <p>{'Click on a row within the "Matched Videos" table to view timestamps and analytics for the match.'}</p>
          </div>
        </Card>
      </div>
    );
  }
}
