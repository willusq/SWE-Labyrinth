import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Modal, Spin, Card, Button, Input } from 'antd';
import axios from 'axios';
import { routerRedux } from 'dva/router';
import store from '../../index';

export default class WatchList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
	video1: '',
	video2: '',
	video3: '',
	video4: '',
	video5: '',
	video6: '',
	video7: '',
	video8: '',
	video9: '',
	video10: '',
	loading: true,
	visible: false
    };
  }

  componentDidMount() {
    var userData = JSON.parse(localStorage.getItem('user'));
    var url = 'http://35.188.175.9:7000/api/currwatchlist/' + userData.schema;
    
    axios.get(url)
    .then((response) => {
        var tmp = response.data.list
	var list = tmp.split('-')
	for (var item in list) {
	    this.setState({['video' + (Number(item) + Number(1))]: list[item]})
	}
    });
    this.setState({loading: false})
  }

  updateVideos = (event) => {
    var video = this.state["video" + event.target.id];
    video = event.target.value;
    var tmp = document.getElementById(String(event.target.id))
    tmp.style.borderColor = "lightgrey"
    this.setState({['video' + event.target.id]: video})
  }

  updateWatchlist = () => {
    this.setState({loading: true});
    var userData = JSON.parse(localStorage.getItem('user'));
    var videos = [];
    for (var i =0 ; i <= 9; i++) {
	videos.push(this.state['video' + (Number(i) + Number(1))]);
    }
    var url  = 'http://35.188.175.9:7000/api/verifywatchlist/' + videos.join('-');
    axios.get(url)
      .then(res => {
	var l = res.data.split('-')
	var valid = true
	for (var i=0;i<l.length;i++){
		if (String(l[i]) == 'false'){
			console.log("invalid")
			valid = false
			var tmp = document.getElementById(String(Number(i) + Number(1)))
			tmp.style.borderColor = "red"
		}
	}
	if (valid){
    		console.log(this.state.valid);
    		var url2  = 'http://35.188.175.9:7000/api/watchlist/' + userData.schema + '/' + videos.join('-');
    		axios.get(url2)
    		.then(res => {
        		console.log(res);
        		console.log(res.data);
			this.success();
      		})
    	}
    })
    this.setState({loading: false});
  }

  success = () => {
    const { dispatch } = store; 
    Modal.success({
      title: 'The watchlist was successfully updated!',
      onOk() {
	dispatch(routerRedux.push('/dashboard/videos'));
      }
    });
  }  

  render() {
    return (
      <div>
	<style>
          { "input:invalid { box-shadow:none; }" }
        </style>
	<Spin spinning={this.state.loading}>
        <Card bordered={true}>
          <div style={{display: 'flex', flexDirection: 'row'}}>
            <h3>{'Watch List'}</h3>
            <Button onClick={this.updateWatchlist} style={{marginLeft: 'auto'}} type="primary">{"Save"}</Button>
          </div>
          <p>{'(10 maximum)'}</p>
          <Input value={ this.state.video1 } onChange={this.updateVideos} id={1} style={{marginTop: 16}} placeholder="Video 1" />
          <Input value={ this.state.video2 } onChange={this.updateVideos} id={2} style={{marginTop: 16}} placeholder="Video 2" />
          <Input value={ this.state.video3 } onChange={this.updateVideos} id={3} style={{marginTop: 16}} placeholder="Video 3" />
	  <Input value={ this.state.video4 } onChange={this.updateVideos} id={4} style={{marginTop: 16}} placeholder="Video 4" />
	  <Input value={ this.state.video5 } onChange={this.updateVideos} id={5} style={{marginTop: 16}} placeholder="Video 5" />
	  <Input value={ this.state.video6 } onChange={this.updateVideos} id={6} style={{marginTop: 16}} placeholder="Video 6" />
	  <Input value={ this.state.video7 } onChange={this.updateVideos} id={7} style={{marginTop: 16}} placeholder="Video 7" />
	  <Input value={ this.state.video8 } onChange={this.updateVideos} id={8} style={{marginTop: 16}} placeholder="Video 8" />
	  <Input value={ this.state.video9 } onChange={this.updateVideos} id={9} style={{marginTop: 16}} placeholder="Video 9" />
	  <Input value={ this.state.video10 } onChange={this.updateVideos} id={10} style={{marginTop: 16}} placeholder="Video 10" />
	</Card>
	</Spin>
      </div>
    );
  }
}
