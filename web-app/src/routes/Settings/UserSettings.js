import React, { PureComponent, Fragment } from 'react';
import moment from 'moment';
import { Modal, Spin, Card, Button, Input, Form, InputNumber, Switch } from 'antd';
const FormItem = Form.Item;
import axios from 'axios';
import { routerRedux } from 'dva/router';
import store from '../../index';

export default class UserSettings extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
        loading: true,
	visible: false
    };

    this.updateUserInfo = this.updateUserInfo.bind(this);
  }

  componentDidMount() {
    
    var userData = JSON.parse(localStorage.getItem('user'));

    var url = 'http://35.188.175.9:7000/api/curruserinfo/' + userData.schema;
    axios.get(url)
    .then((response) => {
        this.setState({fname: response.data.fname})
        this.setState({lname: response.data.lname})
        this.setState({emailAddress: response.data.email})
        this.setState({password: ''})
        this.setState({newPassword: ''})
	this.setState({loading: false})
    });
    //this.setState({fname: userData.fname});
    //this.setState({lname: userData.lname});
    //this.setState({emailAddress: userData.userName});
    //this.setState({password: ''});
    //this.setState({newPassword: ''});
  }

  updateFirstName = (value) => {
    this.setState({fname: value.target.value});
  }

  updateLastName = (value) => {
    this.setState({lname: value.target.value});
  }

  updateEmail = (value) => {
    this.setState({emailAddress: value.target.value});
  }

  updatePassword = (value) => {
    this.setState({password: value.target.value});
  }

  updateNewPassword = (value) => {
    this.setState({newPassword: value.target.value});
  }
  updateUserInfo() {
    var userData = JSON.parse(localStorage.getItem('user'));
    var email = this.state.emailAddress.split("@");
    var domain = email[1].replace('.', '');
    email = email[0] + domain;
    email = 'schema_' + email.replace('@', '');
    var url = 'http://35.188.175.9:7000/api/userinfo/' + userData.schema + '/' + this.state.fname + '/' + this.state.lname + '/' + this.state.emailAddress + '/';
    var user = {};
    user['fullname'] = this.state.fname + ' ' + this.state.lname;
    user['fname'] = this.state.fname;
    user['lname'] = this.state.lname;
    user['userName'] = this.state.emailAddress;
    user['schema'] = email;
    localStorage.setItem('user', JSON.stringify(user));
    if(this.state.password === ''){
	this.enterPassword()
	return
    }
    axios.get(url)
    .then((response) => {
      console.log(response)
    })
    .catch((error) => {
      console.log(error)
    })
	var authEmail=userData.userName;
	var authPass=this.state.password;
   	console.log(this.state.newPassword)
	var url2 = 'http://35.188.175.9:7000/api/updateuser/' + encodeURIComponent(authEmail) + '/' + encodeURIComponent(authPass) + '/'+ email + '/' + this.state.fname + '/' + this.state.lname +'/'+ encodeURIComponent(this.state.emailAddress) + '/' + encodeURIComponent(this.state.newPassword);

	if(this.state.newPassword === ''){
		console.log("no new password")
   		var url2 = 'http://35.188.175.9:7000/api/updateuser/' + encodeURIComponent(authEmail) + '/' + encodeURIComponent(authPass) + '/'+ email + '/' + this.state.fname + '/' + this.state.lname +'/'+ encodeURIComponent(this.state.emailAddress);
	}
	axios.get(url2)
    	.then((response) => {
     		console.log(response)
 		this.success();
    	})
    	.catch((error) => {
      		console.log(error)
    	})
}

  success = () => {
    const { dispatch } = store;
    Modal.success({
      title: 'User settings were successfully updated!',
      onOk() {
        dispatch(routerRedux.push('/dashboard/videos'));
      }
    });
  }

  enterPassword = () => {
    const { dispatch } = store;
    Modal.warning({
      title: 'Please enter your current password.',
    });
  } 

  render() {
    return (
      <div>
	<Spin spinning={this.state.loading}>
        <Card bordered={true}>
          <div style={{display: 'flex', flexDirection: 'row'}}>
            <h3>{'User Settings'}</h3>
            <Button onClick={this.updateUserInfo} style={{marginLeft: 'auto'}} type="primary">{"Save"}</Button>
          </div>
          <br></br>
          <div style={{width: '40%', display: 'flex', flexDirection: 'row'}}>
            <p style={{marginTop: 4, marginRight: 8}}>{'First Name'}</p>
            <Input onChange={this.updateFirstName} value={this.state.fname} placeholder="" />
          </div>
          <div style={{width: '40%', display: 'flex', flexDirection: 'row'}}>
            <p style={{marginTop: 4, marginRight: 8}}>{'Last Name'}</p>
            <Input onChange={this.updateLastName} value={this.state.lname} placeholder="" />
          </div>
          <div style={{width: '40%', display: 'flex', flexDirection: 'row'}}>
            <p style={{marginTop: 4, marginRight: 8}}>{'Email Address'}</p>
            <Input onChange={this.updateEmail} value={this.state.emailAddress} placeholder="" />
          </div>
          <div style={{width: '40%', display: 'flex', flexDirection: 'row'}}>
            <p style={{marginTop: 4, marginRight: 8}}>{'Current Password'}</p>
            <Input onChange={this.updatePassword} value={this.state.password} placeholder="current password" type="password"/>
          </div>
          <div style={{width: '40%', display: 'flex', flexDirection: 'row'}}>
            <p style={{marginTop: 4, marginRight: 8}}>{'New Password'}</p>
            <Input onChange={this.updateNewPassword} value={this.state.newPassword} placeholder="new password" type="password"/>
          </div>
        </Card>
	</Spin>
      </div>
    );
  }
}
