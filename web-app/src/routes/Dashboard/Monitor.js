import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { Spin, Row, Col, Card, Tooltip } from 'antd';
import numeral from 'numeral';
import Authorized from '../../utils/Authorized';
import { Pie, WaterWave, Gauge, TagCloud } from '../../components/Charts';
import NumberInfo from '../../components/NumberInfo';
import CountDown from '../../components/CountDown';
import ActiveChart from '../../components/ActiveChart';
import styles from './Monitor.less';
import axios from 'axios';
import { Chart } from 'react-google-charts';

const { Secured } = Authorized;

const targetTime = new Date().getTime() + 3900000;

@connect(({ monitor, loading }) => ({
  monitor,
  loading: loading.models.monitor,
}))
export default class Monitor extends PureComponent {
  state = {
    certaintyOptions: {
      hAxis: { title: 'Frames' },
      vAxis: { title: 'Percentage', maxValue: 100}
    },
    offsetOptions: {
      hAxis: { title: 'Frames' },
      vAxis: { title: 'Offset (frames)' }
    },
    columnsCertainty: [
      {
	type: 'number',
	label: 'Frames',
      },
      {
	type: 'number', 
	label: 'Certainty',
      },
    ],
    columnsOffset: [
      { 
        type: 'number',
        label: 'Frames',
      },
      { 
        type: 'number', 
        label: 'Offset',
      },
    ],	
    rowsCertainty: [[0,0]],
    rowsOffset: [[0,0]],
    loading: true
  };

  componentDidMount() {
    var userData = JSON.parse(localStorage.getItem('user'));
    var analyticsData = JSON.parse(localStorage.getItem('analytics'));
    var url = 'http://35.188.175.9:7000/api/results/' + userData.schema + '/' + analyticsData.matchid;
    axios.get(url)
    .then((response) => {
	console.log(response);
	var rowsCertaintyData = []
	var rowsOffsetData = []
	for (var item in response.data) {
	   var rowFrames = [response.data[item].split(',').map(Number)[0]]
	   var rowCertainty = [response.data[item].split(',').map(Number)[2] * 100]
	   var rowOffset = [response.data[item].split(',').map(Number)[3]]
	   var certaintyRow = rowFrames.concat(rowCertainty)
	   var offsetRow = rowFrames.concat(rowOffset)
	   rowsCertaintyData.push(certaintyRow)
	   rowsOffsetData.push(offsetRow)
	}
	this.setState({rowsCertainty: rowsCertaintyData})
	this.setState({rowsOffset: rowsOffsetData})
    })
    .catch((error) => {
        console.log(error)
    })
    this.setState({loading: false})
  }

  render() {
    const { monitor, loading } = this.props;
    const { tags } = monitor;
    var analyticsData = JSON.parse(localStorage.getItem('analytics'));

    return (
      <Fragment>
	<Spin spinning={this.state.loading}>
	<Row gutter={24}>
	  <Col style={{ marginBottom: 24}}>
	  	<Card bordered={true}>
		    <h3>{'Match ID: ' + '\n' + analyticsData.matchid}</h3>
	            <h3>{'Source Video: ' + analyticsData.sourcename}</h3>
	  	    <h3>{'Infringing Video: ' + analyticsData.infringename}</h3>
	       </Card>
	  </Col> 
	</Row>
        <Row gutter={24}>
          <Col style={{ marginBottom: 24 }}>
            <Card title="Match Certainty" bordered={false}>
              <Row>
                <Col>
	           <Chart
		        chartType="AreaChart"
	    		options={this.state.certaintyOptions}
        		rows={this.state.rowsCertainty}
       			columns={this.state.columnsCertainty}
        		graph_id="CertaintyChart"
        		width={'100%'}
        		height={'400px'}
      		    /> 
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
	<Row gutter={24}>
          <Col style={{ marginBottom: 24 }}>
            <Card title="Match Offset" bordered={false}>
              <Row>
                <Col>
                   <Chart
                        chartType="AreaChart"
	    		options={this.state.offsetOptions}
                        rows={this.state.rowsOffset}
                        columns={this.state.columnsOffset}
                        graph_id="OffsetChart"
                        width={'100%'}
                        height={'400px'}
                    />
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
	</Spin>
      </Fragment>
    );
  }
}
