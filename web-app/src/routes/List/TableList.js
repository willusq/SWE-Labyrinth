import React, { PureComponent, Fragment } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import moment from 'moment';
import { Spin, Table, Row, Col, Card, Form, Input, Select, Icon, Button, Dropdown, Menu, InputNumber, DatePicker, Modal, message, Badge, Divider } from 'antd';
import StandardTable from '../../components/StandardTable';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import axios from 'axios';
import store from '../../index';

import styles from './TableList.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj => Object.keys(obj).map(key => obj[key]).join(',');
const statusMap = ['default', 'processing', 'success', 'error'];
const status = ['closed', 'running', 'online', 'abnormal'];
const watchColumns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name'
  }
];

const matchColumns = [
  {
    title: 'Match ID',
    dataIndex: 'matchid',
    key:'matchid'
  },
  {
    title: 'Source Video',
    dataIndex: 'sourcename',
    key: 'sourcename'
  },
  {
    title: 'Infringing Video',
    dataIndex: 'infringename',
    key: 'infringename'
  }
];

@connect(({ rule, loading }) => ({
  rule,
  loading: loading.models.rule,
}))
@Form.create()
export default class TableList extends PureComponent {
  state = {
    expandForm: false,
    selectedRows: [],
    formValues: {},
    currWatchData: [],
    matchData: [],
    loading: true
  };

  componentDidMount() {
    const { dispatch } = this.props;
    if (JSON.parse(localStorage.getItem('user')) == null) {
	dispatch(routerRedux.push('/user/login'));
	return
    }	  
    var userData = JSON.parse(localStorage.getItem('user'));
    var url = 'http://35.188.175.9:7000/api/currwatchlist/' + userData.schema;
    axios.get(url)
    .then((response) => {
	if (response.data.list) {
		this.setState({currWatching: response.data.list});
		var currWatchTable = [];
		var currWatching = response.data.list.split('-');
		for (var item in currWatching) {
			if (currWatching[item] != "") {
				currWatchTable.push({key: item, name: currWatching[item]})
			}
		}
		this.setState({currWatchData: currWatchTable});
	} else {
		console.log('no items in watchlist');
	}
    })
    .catch((error) => {
	console.log(error)
    })
    var url = 'http://35.188.175.9:7000/api/results/' + userData.schema;
    axios.get(url)
    .then((response) => {
	if (response.data.length > 0) {
	    var matches = response.data;
	    var matchesTable = [];
	    for (var item in matches) {
		if (matches[item].result != null) {
		    matchesTable.push({matchid: matches[item].id, sourcename: matches[item].source, infringename: matches[item].other});
	    	}
	    }
	    this.setState({matchData: matchesTable});
	} else {
	    console.log('no matches');
	}
    })
    .catch((error) => {
        console.log(error)
    })
    this.setState({loading: false})
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'rule/fetch',
      payload: params,
    });
  }

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'rule/fetch',
      payload: {},
    });
  }

  toggleForm = () => {
    this.setState({
      expandForm: !this.state.expandForm,
    });
  }

  handleMenuClick = (e) => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'rule/remove',
          payload: {
            no: selectedRows.map(row => row.no).join(','),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  }

  handleSelectRows = (rows) => {
    this.setState({
      selectedRows: rows,
    });
  }

  handleSearch = (e) => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'rule/fetch',
        payload: values,
      });
    });
  }

  handleModalVisible = (flag) => {
    this.setState({
      modalVisible: !!flag,
    });
  }

  handleAdd = (fields) => {
    this.props.dispatch({
      type: 'rule/add',
      payload: {
        description: fields.desc,
      },
    });

    message.success('添加成功');
    this.setState({
      modalVisible: false,
    });
  }

  handleRowClick = (params) => {
    const { dispatch } = store;
    localStorage.setItem('router', JSON.stringify(params))
    dispatch(routerRedux.push('/dashboard/results'));
  }

  render() {
    const { rule: { data }, loading } = this.props;
    const { selectedRows, currWatchData, matchData } = this.state;

    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };

    return (
      <div>
	<Spin spinning={this.state.loading}>
        <Card bordered={true}>
          <h3>Currently Monitoring</h3>
          <div className={styles.tableList}>
            <Table
              bordered={true}
              selectedRows={selectedRows}
              loading={loading}
              dataSource={currWatchData}
              columns={watchColumns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
        <Card style={{marginTop: 16}} bordered={true}>
          <h3>Matched Videos</h3>
          <div className={styles.tableList}>
            <Table
              bordered={true}
              selectedRows={selectedRows}
              loading={loading}
              dataSource={matchData}
              columns={matchColumns}
              onRowClick={this.handleRowClick}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
	</Spin>
      </div>
    );
  }
}
