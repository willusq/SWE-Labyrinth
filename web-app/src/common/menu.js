import { isUrl } from '../utils/utils';

const menuData = [{
  name: 'Videos',
  icon: 'video-camera',
  path: 'dashboard/videos',
}, {
  name: 'Settings',
  icon: 'setting',
  path: 'settings',
  children: [{
    name: 'Watch List',
    path: 'watchlist',
  }, {
    name: 'Detection Settings',
    path: 'detection',
  }, {
    name: 'User Settings',
    path: 'users',
  }],
}];

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map((item) => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const getMenuData = () => formatter(menuData);
