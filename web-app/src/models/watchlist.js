import { updateWatchlist } from '../services/api';

export default {
  namespace: 'watchlist',

  state: {
    data: {
      videos: []
    },
  },

  effects: {
    *submit(_, { call, put }) {
      const response = yield call(updateWatchlist);
      yield put({
        type: 'save',
        payload: response,
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};
