#!/bin/bash
keys=(`pwgen 10 20`)
for i in "${keys[@]}"
do
	echo $i >> "$LAB_HOME/global/registration-keys"
done
