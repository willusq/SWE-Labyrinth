#!/bin/bash

while [ 1 -eq 1 ]
do
    # Instance Config Section
	rawjson=`curl 35.188.175.9:7000/api/updateconfig`
	curl 35.188.175.9:7000/api/updateconfig/delete
	schemas=`echo $rawjson | jsawk -n 'out(this.schema)'`
	ref=`echo $rawjson | jsawk -n 'out(this.reflect)'`
	qual=`echo $rawjson | jsawk -n 'out(this.density)'`

	oldIFS=$IFS

	IFS=$'\n'
	schemaArr=($schemas)
	refArr=($ref)
	qualArr=($qual)

	IFS=$oldIFS

	echo
	echo "Instance Config:"
	for (( i=0; i<${#schemaArr[@]}; i++ ))
	do
		su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"UPDATE ${schemaArr[$i]}.config SET reflection='${refArr[$i]}', quality='${qualArr[$i]}';\"" - postgres 
	   #echo "${schemaArr[$i]} | ${refArr[$i]} | ${qualArr[$i]}"
	done
	echo


	# Watch List Section
	rawjson=`curl 35.188.175.9:7000/api/watchlist`
	curl 35.188.175.9:7000/api/watchlist/delete
	schemas=`echo $rawjson | jsawk -n 'out(this.schema)'`

	schemaArr=($schemas)
	listArr=`echo $rawjson | jsawk 'return out(this.list)' | sed "s/\[//g; s/\]//g"`
	echo "Watch List Config:"
	echo $listArr
	for (( i=0; i<${#schemaArr[@]}; i++ ))
	do
		su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"UPDATE ${schemaArr[$i]}.watchlist SET list='$listArr';\"" - postgres 
	done
	echo

	# User Settings Section
	rawjson=`curl 35.188.175.9:7000/api/userinfo`
	curl 35.188.175.9:7000/api/userinfo/delete
	schemas=`echo $rawjson | jsawk -n 'out(this.schema)'`
	emails=`echo $rawjson | jsawk -n 'out(this.email)'`
	fnames=`echo $rawjson | jsawk -n 'out(this.fname)'`
	lnames=`echo $rawjson | jsawk -n 'out(this.lname)'`

	schemaArr=($schemas)
	emailArr=($emails)
	fnameArr=($fnames)
	lnameArr=($lnames)

	echo "User info Config:"
	for (( i=0; i<${#schemaArr[@]}; i++ ))
	do
		tmp1="schema_"`echo ${emailArr[$i]} |  sed 's/@//g; s/\.//g'`
		tmp2=${schemaArr[$i]}
		echo $tmp1
		echo $tmp2
		if [ "$tmp1" == "$tmp2" ];
		then
			su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"UPDATE ${schemaArr[$i]}.userinfo SET email='${emailArr[$i]}', fname='${fnameArr[$i]}', lname='${lnameArr[$i]}';\"" - postgres 
		else
			su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"ALTER SCHEMA $tmp2 RENAME TO $tmp1;\"" - postgres 
			su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"UPDATE $tmp1.userinfo SET email='${emailArr[$i]}', fname='${fnameArr[$i]}', lname='${lnameArr[$i]}';\"" - postgres 


			echo $tmp1
		fi

		#su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"UPDATE ${schemaArr[$i]}.watchlist SET email='${schemaArr[$i]}', fname='${fnameArr[$i]}', lname='${lnameArr[$i]}';\"" - postgres 

	done
	sleep 1
done
