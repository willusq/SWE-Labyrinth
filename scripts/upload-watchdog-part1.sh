#!/bin/bash


while [ 1 -eq 1  ]
do

  uploadsArr=(`ls $LAB_HOME/uploads/ | xargs`)

  for i in "${uploadsArr[@]}"
  do
  	  #newPath="$LAB_HOME/mature-uploads/"`md5sum "$LAB_HOME/uploads/$i" | awk '{print $1}'`
	  newPath="$LAB_HOME/mature-uploads/$i"
	  oldPath="$LAB_HOME/uploads/$i"
	  mv $oldPath $newPath
	  echo $i >> $LAB_HOME/global/labyrinth-queue
  done

  sleep 1

done
