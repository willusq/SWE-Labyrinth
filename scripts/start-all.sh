#!/bin/bash

$LAB_HOME/scripts/configWatchdog.sh &>> /dev/null &
$LAB_HOME/scripts/instanceMaster.sh  &>> /dev/null &
$LAB_HOME/scripts/confirmation-emails.sh &>> /dev/null &
$LAB_HOME/scripts/upload-watchdog-part1.sh &>> /dev/null &
$LAB_HOME/scripts/upload-watchdog-part2.sh &>> /dev/null &

exit 0
