#!/bin/bash

while [ 1 -eq 1 ]
do

  rawjson=`curl 35.188.175.9:7000/api/confirmationemails`
  curl 35.188.175.9:7000/api/confirmationemails/delete

  emails=(`echo $rawjson | jsawk -n 'out(this.email)' | xargs`)

  urls=(`echo $rawjson | jsawk -n 'out(this.url)' | xargs`)

  for (( i=0; i<${#emails[@]}; i++ ))
  do
	  messageBody=$"Weclome to Labyrinth! Please click the following link to complete registration: ${urls[$i]}"
	
	  echo $messageBody | mail -a "From: Labyrinth <labyrinth.wit@gmail.com>"  -s "New Instance Confirmation" ${emails[$i]}
  done

  #curl 35.188.175.9:7000/api/confirmationemails/delete
  sleep 1
done
