#!/bin/bash	
while [ 1 -eq 1 ]
do
  rawjson=`curl 35.188.175.9:7000/api/newinstance`
  curl 35.188.175.9:7000/api/newinstance/delete
  schemas=`echo $rawjson | jsawk -n 'out(this.schema)'`
  emails=`echo $rawjson | jsawk -n 'out(this.email)'`
  fnames=`echo $rawjson | jsawk -n 'out(this.fname)'`
  lnames=`echo $rawjson | jsawk -n 'out(this.lname)'`
  keys=`echo $rawjson | jsawk -n 'out(this.regkey)'`

  schemaArr=($schemas)
  emailArr=($emails)
  fnameArr=($fnames)
  lnameArr=($lnames)
  keyArr=($keys)

  for (( i=0; i<${#schemaArr[@]}; i++ ))
  do
			
    keyExists=`cat "$LAB_HOME/global/registration-keys" | grep "^${keyArr[$i]}$"`

    su -c 'PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c "\dn"' - postgres | grep ${schemaArr[$i]}



    if [ $? -ne 0 ] && [ ! -z $keyExists ];
    then

      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"CREATE SCHEMA ${schemaArr[$i]};\"" - postgres 
      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"CREATE TABLE ${schemaArr[$i]}.results (id int, result text, sections text, source varchar(50), other varchar(50));;\"" - postgres 
      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"CREATE TABLE ${schemaArr[$i]}.config (reflection boolean, quality varchar(15));\"" - postgres 
      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"CREATE TABLE ${schemaArr[$i]}.watchlist (list text);\"" - postgres 
      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"CREATE TABLE ${schemaArr[$i]}.userinfo (email varchar(50), fname varchar(15), lname varchar(15));;\"" - postgres 

      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"INSERT INTO ${schemaArr[$i]}.config (reflection, quality) VALUES ('0','1');\"" - postgres 
      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"INSERT INTO ${schemaArr[$i]}.watchlist (list) VALUES ('---------');\"" - postgres 
      su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"INSERT INTO ${schemaArr[$i]}.userinfo (email, fname, lname) VALUES ('${emailArr[$i]}','${fnameArr[$i]}','${lnameArr[$i]}');\"" - postgres 

      #su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"INSERT INTO ${schemaArr[$i]}.results (id, result, sections, source, other) VALUES ('0','null','null','null','null');\"" - postgres

      #sed -i "s/^${keyArr[$i]}$//g" $LAB_HOME/global/registration-keys
      sed -i -n "/${keyArr[$i]}/!p" $LAB_HOME/global/registration-keys
    fi
  done
sleep 1
done
