#!/bin/bash
while [ 1 -eq 1 ]
do		
	match=`ps ax | grep matching-algorithm | grep -v grep`
	nextUp=`cat $LAB_HOME/global/labyrinth-queue | head -n 1`
	qualityArr=("0" "1" "2");
	echo $match
	
	while ! [ -z "$match" ] 
	do
		match=`ps ax | grep matching-algorithm | grep -v grep`
		echo waiting
		sleep 1
	done

	date

	while [ -z $nextUp ] 
	do
		echo No Jobs
		nextUp=`cat $LAB_HOME/global/labyrinth-queue | head -n 1`
		sleep 1
	done
	
	
	for i in "${qualityArr[@]}"
	do
		$LAB_HOME/bin/preprocessor $nextUp $i
		echo
	done
	
	sed -i 1d "$LAB_HOME/global/labyrinth-queue"

	schemaArr=(`su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \"\dn;\"" - postgres | grep schema_ | awk '{print $1}' | xargs`)
	core=`expr $(nproc) / "${#schemaArr[@]}"`
	coreCount=`expr $core / 2`
	for i in "${schemaArr[@]}"
	do
		watchlist=`su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \" SELECT * FROM $i.watchlist;\"" - postgres | xargs | awk '{print $(NF-2)}'`
		if [ $watchlist != "null" ]; then
			watchlist=`su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \" SELECT * FROM $i.watchlist;\"" - postgres | xargs | awk '{print $(NF-2)}'`
			density=`su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \" SELECT * FROM $i.config;\"" - postgres | xargs | awk '{print $(NF-2)}'`
			reflect=`su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \" SELECT * FROM $i.config;\"" - postgres | xargs | awk '{print $(NF-4)}'`
			watchlist=`echo $watchlist | sed 's/-/ /g'`
			len=`echo $watchlist | wc -w`
			echo
			echo $len
			echo
			maxTableID=`su -c "PGPASSWORD=ohk6shailiMu1ahw0Qui psql -d labyrinth -c \" SELECT MAX(id) from $i.results; \"" - postgres | tail -n3 | head -n1`
			nextID=`expr $maxTableID + 1`
			nextRefID=`expr $nextID + $len`
			nextRefID=`expr $nextRefID + 1`
			if [ $reflect == 't' ]; then

				toRun="$LAB_HOME/bin/matching-algorithm $density $i $nextUp '$watchlist' `echo $watchlist | wc -w` $nextRefID rlab $coreCount "
				toRun2="$LAB_HOME/bin/matching-algorithm $density $i $nextUp '$watchlist' `echo $watchlist | wc -w` $nextID lab $coreCount "
				
				echo $toRun
				echo $toRun2
				eval $toRun &
				eval $toRun2 &
			else 
				toRun="$LAB_HOME/bin/matching-algorithm $density $i $nextUp '$watchlist' `echo $watchlist | wc -w` $nextID lab $core"
				eval $toRun &
			fi
		fi
	done
done
