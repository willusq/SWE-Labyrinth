#!/bin/bash

kill -9 `pgrep -f ./configWatchdog.sh`
kill -9 `pgrep -f ./instanceMaster.sh`
kill -9 `pgrep -f ./confirmation-emails.sh`
kill -9 `pgrep -f ./upload-watchdog-part1.sh`
kill -9 `pgrep -f ./upload-watchdog-part2.sh`

exit 0
