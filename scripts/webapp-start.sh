#!/bin/bash

PORT=7000 $LAB_HOME/webapp-backend/bin/www &>> /dev/null &

cd $LAB_HOME/web-app/
npm start &>> /dev/null &

exit 0
